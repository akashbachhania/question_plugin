<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'question_plugin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's(^$WJ8OvRYdY|+KR*f#+6+9k;|8weL60)6{8Fxw7|6~-9<8Fr|N{l)T@k&1@`)G');
define('SECURE_AUTH_KEY',  '&L0:.#rO3o!5:|w,~->fdi-Jz%*>erkT~+j4HfSjFOdY^|+!7@l)-/%8plCAqFmM');
define('LOGGED_IN_KEY',    'Xc@PX9?V{]7Qs>C_OM!nDx-_vaK|Z`{@)dQv?gA8-E e`d.Z6#jWnUf#`36^B?F&');
define('NONCE_KEY',        'S*-sR|kc-(o||gBsN*XOwS(]Q.>Q!Yac-LyCjS-e7Qzbg|hM0Vi=V@Is7p*R1>q`');
define('AUTH_SALT',        'G`7WkPSWFB`lRWZfq%M<:`10EL6&9XDb1(`IUZrW^eoX|<?sEn4eBDc:%jWf-,e8');
define('SECURE_AUTH_SALT', ';46ML&3VJBn%av=`|q~; *1%1CL&pxCC`2Qz3Vdx;WeEupZ)Ubk6@c^Vt0}E5OEs');
define('LOGGED_IN_SALT',   '+921PA{-=-q|2A+-=;Qbn3wK%85X9GYxjD0^wIJ?3mo[tLk9<13vV-.JvoEk67{6');
define('NONCE_SALT',       'LL)qiBCY|bAP}s7hQL|/A[_*7LCyA@`->R5qZzaAJoK^dbQh~*v}y8u.WAxizvLZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
