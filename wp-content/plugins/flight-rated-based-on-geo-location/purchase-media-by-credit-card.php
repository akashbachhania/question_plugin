<?php
@session_start();
error_reporting(E_ALL ^ E_NOTICE);
include_once("../../common/define.php");
include_once("../../classes/class.query.php");
$objQuery=new query();
include_once("../../classes/class.common.php");
$objCmn = new common();
include_once("../../classes/class.fan.php");
$objFan = new fan();
$upload_id=$_GET['upload_id'];
$purchasetype=$_GET['purchasetype'];


if(isset($_POST['payment_info_btn']) && $_POST['payment_info_btn']!='')
{
	$errormessage='';
	$paymentType		=	trim($_POST['paymentType']);
	$creditCardNumber	=	trim($_POST['creditCardNumber']);
	$cvv2Number			=	trim($_POST['cvv2Number']);
	$creditCardType		=	$_POST['creditCardType'];
	$expDateMonth		=	$_POST['expDateMonth'];
	$expDateYear		=	$_POST['expDateYear'];
	$amount				=	$_POST['amount'];
	$upload_id			=	$_POST['upload_id'];
	
	if($creditCardNumber=='') {
		$errormessage.='Please enter your credit card number.<br>';
	}
	
	if($cvv2Number=='') {
		$errormessage.='Please enter your cvv number.<br>';
	}

	if($errormessage=='')
	{
		$_SESSION['amount']			=	$amount;
		$_SESSION['paymentType']		=	$paymentType;
		$_SESSION['creditCardNumber']           =	$creditCardNumber;
		$_SESSION['cvv2Number']			=	$cvv2Number;
		$_SESSION['creditCardType']		=	$creditCardType;
		$_SESSION['expDateMonth']		=	$expDateMonth;
		$_SESSION['expDateYear']		=	$expDateYear;
		$_SESSION['upload_id']			=	$upload_id;
		//header("location:".BASE_URL."fans-panel/payment-receipt");				
	}	
	else
	{
		$showmessage=$errormessage;
		$class="class='error_message'";		
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include_once(BASE_PATH."templates/csshtml.php"); ?>
<script language="JavaScript">
function generateCC(){
var cc_number = new Array(16);
var cc_len = 16;
var start = 0;
var rand_number = Math.random();

switch(document.DoDirectPaymentForm.creditCardType.value)
{
	case "Visa":
		cc_number[start++] = 4;
		break;
	case "Discover":
		cc_number[start++] = 6;
		cc_number[start++] = 0;
		cc_number[start++] = 1;
		cc_number[start++] = 1;
		break;
	case "MasterCard":
		cc_number[start++] = 5;
		cc_number[start++] = Math.floor(Math.random() * 5) + 1;
		break;
	case "Amex":
		cc_number[start++] = 3;
		cc_number[start++] = Math.round(Math.random()) ? 7 : 4 ;
		cc_len = 15;
		break;
}

for (var i = start; i < (cc_len - 1); i++) {
	cc_number[i] = Math.floor(Math.random() * 10);
}

var sum = 0;
for (var j = 0; j < (cc_len - 1); j++) {
	var digit = cc_number[j];
	if ((j & 1) == (cc_len & 1)) digit *= 2;
	if (digit > 9) digit -= 9;
	sum += digit;
}

var check_digit = new Array(0, 9, 8, 7, 6, 5, 4, 3, 2, 1);
cc_number[cc_len - 1] = check_digit[sum % 10];

document.DoDirectPaymentForm.creditCardNumber.value = "";
for (var k = 0; k < cc_len; k++) {
	document.DoDirectPaymentForm.creditCardNumber.value += cc_number[k];
}
}
</script>
<body>

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/header.php"); 
}
?>

<div id="content-wrapper">

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/sidebar.php");
}
?>

<div id="right-cnt-wrap">
	<div class="artist-view-profile-contentarea">
	<div class="pageheader"><h1>Purchase Media</h1></div>  
<div class="<?=$class?>"><?=$showmessage?></div>      
<?php  

if(isset($purchasetype) && $purchasetype=='album') 
{
	$fetch=mysql_query("select * from alubm_ep where album_id='".$upload_id."' limit 0,1 ");
	if(mysql_num_rows($fetch)>0)
	{
		$result=mysql_fetch_array($fetch);
	}
}
else
{
	$fetch=mysql_query("select * from user_uploads where upload_id='".$upload_id."' limit 0,1 ");
	if(mysql_num_rows($fetch)>0)
	{
		$result=mysql_fetch_array($fetch);
	}
}
?>
<div class="purchase-media-credit-wrap">
<form method="POST" action="<?=BASE_URL."purchase-panel/payment-receipt";?>" name="DoDirectPaymentForm">
<input type="hidden" name="paymentType" value="sale" />
<input type="hidden" name="amount" value="<?=$result['upload_amount'];?>" />
<input type="hidden" name="upload_id" value="<?=$upload_id;?>" />
<input type="hidden" name="purchasetype" value="<?=$purchasetype?>" />
<table>
<tr>
    <td><span>Card Type:</span></td>
    <td>
        <select name="creditCardType">
            <option selected="true"></option>
            <option value="Visa">Visa</option>
            <option value="MasterCard">MasterCard</option>
            <option value="Discover">Discover</option>
            <option value="Amex">American Express</option>
        </select>
    </td>
</tr>
<tr>
    <td><span>Card Number:</span></td>
    <td><input type=text size="19" maxlength="19" name="creditCardNumber"></td>
</tr>
<tr>
    <td><span>Expiration Date:</span></td>
    <td>
        <select class="purchase-select-small" name="expDateMonth">
            <option></option>
        <?php for($i=1; $i<13; $i++) { if($i<10) $i="0".$i;	?>
        <option value="<?=$i?>"><?=$i?></option>
        <?php } ?>
        </select>
        <select class="purchase-select-small" name="expDateYear">
            <option></option>
        <?php for($i=date("Y"); $i<date("Y")+50; $i++) { ?>
        <option value="<?=$i?>"><?=$i?></option>
        <?php } ?>
        </select>
    </td>
</tr>
<tr>
    <td><span>Card Verification Number:</span></td>
    <td><input type=text size="3" maxlength="4" name="cvv2Number" value=""></td>
</tr>

<tr>
    <td><span>Zip Code:</span></td>
    <td><input type=text size="19" maxlength="19" name="zipcode"></td>
</tr>    
<tr>
<td>&nbsp;</td>
<td><input class="purchese-submit-btn" type="submit" name="payment_info_btn" value="Submit" /></td>
</tr>
</table>

</form>        
</div>
	</div>
</div>
</div> 
<div class="clear"></div>
<?php include(BASE_PATH."/templates/footer.php"); ?>
</body>
</html>
<script language="javascript">
	generateCC();
</script>