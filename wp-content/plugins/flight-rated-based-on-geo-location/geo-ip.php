<?php   
/* 
Plugin Name: Flight rate based on geo loacation.
Plugin URI: http://www.indiainfotech.com 
Description: Rates calculator based on source and destination address
Author: C. Developer
Version: 1.0 
Author URI: http://www.indiainfotech.com 
*/  

include_once('common.php');

wp_enqueue_script( 'rates-distance-calculator', plugin_dir_url( __FILE__ ) . 'js/ajax.js', array( 'jquery' ) );
wp_localize_script( 'rates-distance-calculator', 'RateCalc', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
//wp_localize_script( 'rates-distance-calculator', 'CalApi', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

add_action('admin_menu', 'address_lat_long_menu');

add_action( 'wp_ajax_nopriv_address-submit', 'get_distance_and_rates' );
add_action( 'wp_ajax_address-submit', 'get_distance_and_rates' );

add_action( 'wp_ajax_nopriv_call-api', 'calling_api' );
add_action( 'wp_ajax_call-api', 'calling_api' );


function get_distance_and_rates() {  
	global $wpdb;	
	$source = $_POST['source_address'];		
	$destination = $_POST['destination_address'];
        $person=$_POST['person'];
        

	// Getting the latitude an longitude of the source and the destination
	$s_latlong = get_latlong($source);
	$d_latlong = get_latlong($destination);
	
	$distance_in_km = ceil(distance($s_latlong['lat'],$s_latlong['long'],$d_latlong['lat'],$d_latlong['long'],'K'));
	
	if($distance_in_km > 0 and $distance_in_km < 5000 and $person<=3){
		$rates_object = get_total_rates($distance_in_km);
		$rates_per_km = $rates_object[0]->rates;
		// Calculate the total amount which will be applied for the distance
		$total_amount = number_format(ceil($distance_in_km * $rates_per_km),2,'.',',');
		$response_array = array('calc_response_code'=>200,'total_distance'=>$distance_in_km,'rate_per_km'=>$rates_per_km,'total_amount'=>$total_amount);
	}
        else if($distance_in_km > 0 and $distance_in_km < 5000 and $person>3){
            $extra_amount=($person-3)*(10);
            $rates_object = get_total_rates($distance_in_km);
            $rates_per_km = $rates_object[0]->rates;
            // Calculate the total amount which will be applied for the distance
            $total_amount = number_format(ceil(($distance_in_km * $rates_per_km )+ $extra_amount),2,'.',',');
            $response_array = array('calc_response_code'=>200,'total_distance'=>$distance_in_km,'rate_per_km'=>$rates_per_km,'total_amount'=>$total_amount);
        }
        else{
		$response_array = array('calc_response_code'=>400);
	}

    $response = json_encode($response_array);

//    header( "Content-Type: application/json" );
    print_r($response);
    exit;
}

//Api function for action start
function calling_api(){
    //-------------------------------------------------
// When you integrate this code
// look for TODO as an indication
// that you may need to provide a value or take action
// before executing this code
//-------------------------------------------------

require_once ("paypalplatform.php");


// ==================================
// PayPal Platform Parallel Payment Module
// ==================================

// Request specific required fields
$actionType			= "PAY";
$cancelUrl			= "http://gyanganit.com/paypal-adaptive/5/action.php?action=cancel";	// TODO - If you are not executing the Pay call for a preapproval,
												//        then you must set a valid cancelUrl for the web approval flow
												//        that immediately follows this Pay call
$returnUrl			= "http://gyanganit.com/paypal-adaptive/5/action.php?action=return";	// TODO - If you are not executing the Pay call for a preapproval,
												//        then you must set a valid returnUrl for the web approval flow
												//        that immediately follows this Pay call
$currencyCode		= "USD";

// A parallel payment can be made among two to six receivers
// TODO - specify the receiver emails
//        remove or set to an empty string the array entries for receivers that you do not have
$receiverEmailArray	= array(
		'paypro_1340622395_biz@gmail.com',
		'sky260_1324291605_biz@gmail.com'
		);

// TODO - specify the receiver amounts as the amount of money, for example, '5' or '5.55'
//        remove or set to an empty string the array entries for receivers that you do not have
$receiverAmountArray = array(
		'11',
		'21',
		);

// for parallel payment, no primary indicators are needed, so set empty array
$receiverPrimaryArray = array();

// TODO - Set invoiceId to uniquely identify the transaction associated with each receiver
//        set the array entries with value for receivers that you have
//		  each of the array values must be unique
$receiverInvoiceIdArray = array(
		'',
		'',
		'',
		'',
		'',
		''
		);

// Request specific optional fields
//   Provide a value for each field that you want to include in the request, if left as an empty string the field will not be passed in the request
$senderEmail					= "";		// TODO - If you are executing the Pay call against a preapprovalKey, you should set senderEmail
											//        It is not required if the web approval flow immediately follows this Pay call
$feesPayer						= "";
$ipnNotificationUrl				= "http://gyanganit.com/paypal-adaptive/5/ipn.php";
$memo							= "Testing paypal adaptive method for fishaband";		// maxlength is 1000 characters
$pin							= "";		// TODO - If you are executing the Pay call against an existing preapproval
											//        the requires a pin, then you must set this
$preapprovalKey					= "";		// TODO - If you are executing the Pay call against an existing preapproval, set the preapprovalKey here
$reverseAllParallelPaymentsOnError	= "";	// TODO - Set this to "true" if you would like each parallel payment to be reversed if an error occurs
											//        defaults to "false" if you don't specify
$trackingId						= generateTrackingID();	// generateTrackingID function is found in paypalplatform.php

//-------------------------------------------------
// Make the Pay API call
//
// The CallPay function is defined in the paypalplatform.php file,
// which is included at the top of this file.
//-------------------------------------------------
$resArray = CallPay ($actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
						$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
						$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
						$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId
);

$ack = strtoupper($resArray["responseEnvelope.ack"]);
if($ack=="SUCCESS")
{
	if ("" == $preapprovalKey)
	{
		// redirect for web approval flow
		$cmd = "cmd=_ap-payment&paykey=" . urldecode($resArray["payKey"]);
		RedirectToPayPal ( $cmd );

	}
	else
	{
		// payKey is the key that you can use to identify the result from this Pay call
		$payKey = urldecode($resArray["payKey"]);
		// paymentExecStatus is the status of the payment
		$paymentExecStatus = urldecode($resArray["paymentExecStatus"]);
	}
} 
else  
{
	//Display a user friendly Error on the page using any of the following error information returned by PayPal
	//TODO - There can be more than 1 error, so check for "error(1).errorId", then "error(2).errorId", and so on until you find no more errors.
	$ErrorCode = urldecode($resArray["error(0).errorId"]);
	$ErrorMsg = urldecode($resArray["error(0).message"]);
	$ErrorDomain = urldecode($resArray["error(0).domain"]);
	$ErrorSeverity = urldecode($resArray["error(0).severity"]);
	$ErrorCategory = urldecode($resArray["error(0).category"]);
	
	echo "Preapproval API call failed. ";
	echo "Detailed Error Message: " . $ErrorMsg;
	echo "Error Code: " . $ErrorCode;
	echo "Error Severity: " . $ErrorSeverity;
	echo "Error Domain: " . $ErrorDomain;
	echo "Error Category: " . $ErrorCategory;
}

}

//Api function for action end



define( 'DISTANCE_RATES_CALCULATOR_PATH', plugin_dir_path(__FILE__) );

function address_lat_long_menu() {
   add_menu_page('Set Rates', 'Set Rates', 'administrator', 'Set Rates', 'distance_rates','', 66);
}


function distance_rates(){
global $wpdb;


if(isset($_POST['add_km'])){
	$skm = $_POST['skm'];
	$ekm = $_POST['ekm'];
	$rates = $_POST['rate'];	
	$sql = "insert into ".$wpdb->prefix . "geo_ip_rates values('','$skm','$ekm','$rates')";
	$wpdb->query($sql);
}

if(isset($_POST['del_km'])){
	$id = $_POST['del_id'];
	$sql = "delete from ".$wpdb->prefix . "geo_ip_rates where id='$id'";
	$wpdb->query($sql);
}


	$form = '<h2>Set Rates for the distances</h2><h3>Just use [flightrate] shortcode in post , page or widget.</h3><br>e.g. 0km - 50km : $10 / km<br />
        <form name="distance_calculator" id="distance_calculator" action="" method="post">
            <table width="70%" border=1 cellspacing=0 cellpadding=15>
                <tr>
                    <th>Serial No</th>
                    <th>Start KM</th>
                    <th>End KM</th>
                    <th>Rates/km</th>
                    <th>Action</th>
                </tr>
                
                <tr>
                    <th>&nbsp;</th>
                    <th><input type="text" name="skm" size="20" ></th>
                    <th><input type="text" name="ekm" size="20" ></th>
                    <th><input type="text" name="rate" size="20" ></th>
                    <th><input type="submit" name="add_km" value="Add"></th>
                </tr>';

	$sql = "select * from ".$wpdb->prefix . "geo_ip_rates";
	$results = $wpdb->get_results($sql,OBJECT);
$ctr = 0;
foreach($results as $row){
		$ctr++;
		$form.='<tr><th>'.$ctr.'</th><th>'.$row->start_km.' km</th><th>'.$row->range_km.' km</th><th>$ '.$row->rates.'</th><th><input type="hidden" name="del_id" 
		value="'.$row->id.'"/><input type="submit" name="del_km" value="Delete"></th></tr>';
}

$form.='</table></form>';

echo $form;
}	

		
function geo_ip_rates(){
global $wpdb;
$sql = "CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "geo_ip_rates (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_km` smallint(6) NOT NULL,
  `range_km` smallint(6) NOT NULL,
  `rates` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

$wpdb->query($sql);

$sql1="CREATE TABLE IF NOT EXISTS ". $wpdb->prefix . "geo_info (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `name` varchar(255),
  `phone` varchar(255) ,
  `email` varchar(255) ,
  `date` varchar(255) ,
  `hour` varchar(255) ,
  `flight_train_no` varchar(255) ,
  `card_type` varchar(255) NOT NULL,
  `card_no` varchar(255) NOT NULL,
  `expiration_date` varchar(255) NOT NULL,
  `card_ver_no` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `no_of_person` int(11) NOT NULL,
  `expDateMonth` int(11) NOT NULL,
  `expDateYear` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

$wpdb->query($sql1);
}

register_activation_hook(__FILE__,'geo_ip_rates');

add_action('wp_ajax_my_unique_action','calc_rates',50,3);

add_filter('widget_text', 'do_shortcode');		
?>