<?php
@session_start();
error_reporting(E_ALL ^ E_NOTICE);
include_once("../../common/define.php");
include_once("../../classes/class.query.php");
$objQuery=new query();
include_once("../../classes/class.common.php");
$objCmn = new common();
include_once("../../classes/class.fan.php");
$objFan = new fan();
$upload_id=$_GET['upload_id'];
$purchasetype=$_GET['ptype'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include_once(BASE_PATH."templates/csshtml.php"); ?>
<body>

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/header.php"); 
}
?>


<div id="content-wrapper">

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/sidebar.php");
}
?>

<div id="right-cnt-wrap">
	<div class="artist-view-profile-contentarea">
	<div class="pageheader"><h1>Purchase Media</h1></div>  
<div class="<?=$class?>"><?=$showmessage?></div>      
<table width="100%">
<tr>
    <td>
    	By Credit Card<input type="radio" name="paymentby" value="cc" class="selectpaymenttype" />
    	By Paypal<input type="radio" name="paymentby" value="paypal" class="selectpaymenttype" />
    </td>
</tr>
</table>
</div>
</div>
</div> 
<div class="clear"></div>
<?php include(BASE_PATH."/templates/footer.php"); ?>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$(".selectpaymenttype").click(function(){
			var ptype=this.value;
			var purtype='<?=$purchasetype?>';
			if(ptype=='paypal') 
			{
				window.location.href='<?=BASE_URL."fan/purchase-by-credi-card/paypal_ipn.php?upload_id=".$upload_id;?>&purchasetype=<?=$purchasetype?>';
			} 
			else 
			{
				if(purtype=='') {
					window.location.href='<?=BASE_URL."purchase-panel/cc/purchase-media/".$upload_id;?>';
				} else {
					window.location.href='<?=BASE_URL."purchase-panel/cc/purchase-media/".$upload_id;?>/<?=$purchasetype?>';
				}
			}
		});
	});
</script>