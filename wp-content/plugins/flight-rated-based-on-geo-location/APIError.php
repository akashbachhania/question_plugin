<?php
/*************************************************
APIError.php

Displays error parameters.

Called by DoDirectPaymentReceipt.php, TransactionDetails.php,
GetExpressCheckoutDetails.php and DoExpressCheckoutPayment.php.

*************************************************/

@session_start();
error_reporting(E_ALL ^ E_NOTICE);
include_once("../../common/define.php");
include_once("../../classes/class.query.php");
$objQuery=new query();
include_once("../../classes/class.common.php");
$objCmn = new common();
include_once("../../classes/class.fan.php");
$objFan = new fan();
$resArray=$_SESSION['reshash']; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include_once(BASE_PATH."templates/csshtml.php"); ?>
<body>

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/header.php"); 
}
?>

<div id="content-wrapper">

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/sidebar.php");
}
?>

<div id="right-cnt-wrap">
	<div class="artist-view-profile-contentarea">
	<div class="pageheader"><h1>Paypal error</h1></div>  
    <table width="280">
    <tr><td colspan="2" class="header">The PayPal API has returned an error!</td></tr>
    <?php  
    if(isset($_SESSION['curl_error_no'])) 
    { 
        $errorCode= $_SESSION['curl_error_no'] ;
        $errorMessage=$_SESSION['curl_error_msg'] ;	
        ?>
        <tr>
            <td>Error Number:</td>
            <td><?php echo $errorCode; ?></td>
        </tr>
        <tr>
            <td>Error Message:</td>
            <td><?php echo $errorMessage; ?></td>
        </tr>
    <?php 
    } 
    else 
    { 
        foreach($resArray as $key => $value) 
        {
            echo "<tr><td> $key:</td><td>$value</td>";
        }	
    }
    ?>
    </table>       
	</div>
</div>
</div> 
<div class="clear"></div>
<?php include(BASE_PATH."/templates/footer.php"); ?>
</body>
</html>