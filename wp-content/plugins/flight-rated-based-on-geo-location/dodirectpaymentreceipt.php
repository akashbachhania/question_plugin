<?php
/***********************************************************
DoDirectPaymentReceipt.php

Submits a credit card transaction to PayPal using a
DoDirectPayment request.

The code collects transaction parameters from the form
displayed by DoDirectPayment.php then constructs and sends
the DoDirectPayment request string to the PayPal server.
The paymentType variable becomes the PAYMENTACTION parameter
of the request string.

After the PayPal server returns the response, the code
displays the API request and response in the browser.
If the response from PayPal was a success, it displays the
response parameters. If the response was an error, it
displays the errors.

Called by DoDirectPayment.php.

Calls CallerService.php and APIError.php.

***********************************************************/

@session_start();
error_reporting(E_ALL ^ E_NOTICE);


require_once 'CallerService.php';


/**
* Get required parameters from the web form for the request
*/
$paymentType 		=	urlencode( $_POST['paymentType']);
$firstName 		=	urlencode( $_POST['firstName']);
$creditCardType 	=	urlencode( $_POST['creditCardType']);
$creditCardNumber 	=	urlencode($_POST['creditCardNumber']);
$expDateMonth 		=	urlencode( $_POST['expDateMonth']);
// Month must be padded with leading zero
$padDateMonth 		=	str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
$expDateYear 		=	urlencode( $_POST['expDateYear']);
$cvv2Number 		=	urlencode($_POST['cvv2Number']);
//$amount 		=	urlencode($_POST['amount']);
$amount=1.00;
$currencyCode		=	"USD";
//$zipcode                =       urlencode($_POST['zipcode']);
/* Construct the request string that will be sent to PayPal.
The variable $nvpstr contains all the variables and is a
name value pair string with & as a delimiter */
$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".$padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";

/* Make the API call to PayPal, using API signature.
The API response is stored in an associative array called $resArray */
$resArray=hash_call("doDirectPayment",$nvpstr);

/* Display the API response back to the browser.
If the response from PayPal was a success, display the response parameters'
If the response was an error, display the errors received using APIError.php.
*/
$ack = strtoupper($resArray["ACK"]);

if($ack!="SUCCESS")  
{
	$_SESSION['reshash']=$resArray;
	$location = BASE_URL."purchase-panel/payment-error";
	header("Location: $location");
}

if($ack=="SUCCESS")
{
	if(isset($_POST['purchasetype']) && $_POST['purchasetype']=='album') 
	{
		$check=mysql_query("select * from fan_listening_lounge where payment_status=1 and fan_id='".$_SESSION['user_id']."' and upload_id='".$_POST['upload_id']."' and 
		purchase_type='".$_POST['purchasetype']."'"); 
		if($check && mysql_num_rows($check)>0) {
		} else {
			$sql="insert into fan_listening_lounge set payment_status=1,payment_date='".date("Y-m-d")."',fan_id='".$_SESSION['user_id']."',upload_id='".$_POST['upload_id']."',
			date_added='".date("Y-m-d")."',purchase_type='".$_POST['purchasetype']."'";
		}
	} 
	else 
	{
		$check=mysql_query("select * from fan_listening_lounge where payment_status=1 and fan_id='".$_SESSION['user_id']."' and upload_id='".$_POST['upload_id']."' 
		and purchase_type='individual' "); 
		if($check && mysql_num_rows($check)>0) {
		} else {
			$sql="insert into fan_listening_lounge set payment_status=1,payment_date='".date("Y-m-d")."',fan_id='".$_SESSION['user_id']."',upload_id='".$_POST['upload_id']."',
			date_added='".date("Y-m-d")."'";
		}
	}
	$in=mysql_query($sql);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include_once(BASE_PATH."templates/csshtml.php"); ?>
<body>

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/header.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/header.php"); 
}
?>

<div id="content-wrapper">

<?php 
if(isset($_SESSION['user_id']) && $_SESSION['type']=='fan')
{ 
	include_once(BASE_PATH."fan/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='artist')
{ 
	include_once(BASE_PATH."artist/template/sidebar.php"); 
}
elseif(isset($_SESSION['user_id']) && $_SESSION['type']=='venue')
{ 
	include_once(BASE_PATH."venue/template/sidebar.php");
}
?>

<div id="right-cnt-wrap">
	<div class="artist-view-profile-contentarea">
	<div class="pageheader"><h1>Thank you for your payment!</h1></div>  
<div class="purchase-media-credit-wrap">
    <table>
	<?php
	if(isset($_POST['purchasetype']) && $_POST['purchasetype']!='') 
	{
		echo '<tr><td>Download</td><td>You can download your purchased album from your download section.</td></tr>';
	} 
	else 
	{
		$fetch=mysql_query("select a.* , b.* from user_uploads a , users b where a.upload_id='".$_POST['upload_id']."' and a.user_id = b.user_id limit 0,1 "); 
		if(mysql_num_rows($fetch)>0)
		{
			$result=mysql_fetch_array($fetch);
			echo '<tr><td><span>Song title : </span></td><td>'.$result['uploaded_file_title'].'</td></tr>';
	
			$file_location=BASE_PATH.UPLOAD_PATH."artist_uploads/".$result['user_id']."/".$result['uploaded_file_name'];
			$file_url=BASE_URL.UPLOAD_PATH."artist_uploads/".$result['user_id']."/".$result['uploaded_file_name'];
			if($result['uploaded_file_name']!='' && file_exists($file_location))
			{
				echo '<tr><td>&nbsp;</td><td><a href="'.$file_url.'" target="_blank" class="increase_download_count" id="'.$result['upload_id'].'" style="font-size:20px;">Click here to download</a></td></tr>';
			}
		}

	}
	?>
    <?php 
    foreach($resArray as $key => $value) {
    	echo "<tr><td><span> $key :</span> </td><td>$value</td>";
    }	
    ?>
    </table>
    </div>           
	</div>
</div>
</div> 
<div class="clear"></div>
<?php include(BASE_PATH."/templates/footer.php"); ?>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
	$(".increase_download_count").click(function(){
		pdata="songid="+this.id;
		$.ajax({
			url		:	"<?=BASE_URL?>fan/increase_download_count.php",
			type	:	"POST",			
			data	:	pdata,
			success	:	function(data) {
                            <?php $redirect=BASE_URL."fans-panel/listening-lounge/audio";?>
                            window.location.href="<?php echo $redirect?>";
			}
		});	
	});
});
</script>