var obj;
function get_distance_and_rates(){

        var data = jQuery('#rate_calculator').serialize();
	data += '&action=address-submit';
	jQuery.post(RateCalc.ajaxurl, data, function(response) {
			obj = jQuery.parseJSON(response);
			jQuery('#rate_calc_container').remove();
			if(obj.calc_response_code==200 && obj.total_amount != null && obj.rate_per_km != null){
				jQuery('#locationField').hide();
                                jQuery('#personal_div').show();
	jQuery('#calc_container').append('<div id="rate_calc_container"><span>Distance Approx : '+ obj.total_distance +'</span><br><span> Rate/KM : $ '+ obj.rate_per_km  +'</span><br><span>Total Amount Approx : $ '+ obj.total_amount +'</span></div>')	;
			}			
	});
}

function insert_info(){
 jQuery("#map").css("display","block");
 	jQuery('#calc_container').hide();
    jQuery('#personal_div').hide();
    jQuery('#dep').val(jQuery('#autocomplete').val());
    jQuery('#arr').val(jQuery('#autocomplete1').val());
    jQuery('#pri').val(obj.total_amount);
    jQuery('#per').val(jQuery('#person').val());
    jQuery('#nam').val(jQuery('#full_name').val());
    jQuery('#phn').val(jQuery('#phone').val());
    jQuery('#ema').val(jQuery('#add').val());
    jQuery('#dat').val(jQuery('#enter_date').val());
    jQuery('#hou').val(jQuery('#enter_hour').val());
    jQuery('#fli_no').val(jQuery('#flight_no').val());
    
    jQuery('#info').show();
	
    //alert('yess');
    //jQuery('#info').append('<span>Departure Address :'+jQuery('#autocomplete').val()+'</span>\n\
    //                        <br><span>Arrival Address : '+jQuery('#autocomplete1').val()+'</span>\n\
    //                        <br><span>Number Of Person : '+jQuery('#person').val()+'</span><br>\n\
    //                        <span>Price : '+obj.total_amount+'</span><br>\n\
    //                        <span>Name :'+jQuery('#full_name').val()+': </span>\n\
    //');
}

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete,autocomplete1;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
      
      autocomplete1 = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete1')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
  
  google.maps.event.addListener(autocomplete1, 'place_changed', function() {
    fillInAddress();
  });
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
  var place = autocomplete1.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
      autocomplete1.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}
// [END region_geolocation]

//Api start
function calling_api(){
    //alert(jQuery(location).attr('href'));
     var data = jQuery('#rate_calculator').serialize();
	data += '&action=call-api';
	jQuery.post(RateCalc.ajaxurl, data, function(response) {
			obj = jQuery.parseJSON(response);
			jQuery('#rate_calc_container').remove();
			if(obj.calc_response_code==200 && obj.total_amount != null && obj.rate_per_km != null){
				jQuery('#locationField').hide();
                               jQuery('#personal_div').show();
	jQuery('#calc_container').append('<div id="rate_calc_container"><span>Distance Approx : '+ obj.total_distance +'</span><br><span> Rate/KM : $ '+ obj.rate_per_km  +'</span><br><span>Total Amount Approx : $ '+ obj.total_amount +'</span></div>')	;
			}			
	});
        
        //window.location = "http://www.google.com";
}
