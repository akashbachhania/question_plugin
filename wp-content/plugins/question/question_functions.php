<?php
//Function to return random string length passed into function
function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

function question_submit_form(){
    global $wpdb;
    
    if(isset($_POST['submit'])){
        unset($_POST['submit']);
        $_POST['unique_code'] = random_string(15);
        $wpdb->insert("wp_question", $_POST);
        $the_page_name = get_option( "answer_page_name" );
        $the_page_id = get_option( 'answer_page_id' );
        //echo $the_page_id;die;
        wp_redirect(get_permalink($the_page_id));
    }
}

function form_creation(){
 
//Adding scripts and css
wp_enqueue_script( 'bootstrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.js', array( 'jquery' ) );
wp_register_style( 'style-css', plugin_dir_url( __FILE__ ) . 'css/style.css' );
wp_enqueue_style('style-css');
wp_register_style( 'bootstrap-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap.css' );
wp_enqueue_style('bootstrap-css');

?>
<div id="dotted_bg"></div>
    
<!--begin container -->
<div id="container">

    <!--begin contact section -->
    <section id="contact">
            
        <!--begin contact_wrapper -->
        <div class="contact_wrapper">
            <!--begin row -->
            <div class="row">
                <!--begin twelvecol -->
                <div class="col-sm-12">
                    <!--begin sevencol -->
                    <div class="twelvecol">
                        <!--begin contact_box -->
                        <div class="col-sm-offset-2 col-sm-8">
                            <!--begin contact form -->
                            <form class="form-horizontal" action="<?php echo admin_url( 'admin.php' ); ?>" method="post">
                                <h4 class="orange">SUBMIT YOUR QUESTIONS</h4>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3">Email</label>
                                    
                                    <div class="col-sm-9">
                                      <input type="email" class="form-control input-bg" placeholder="Email" name="email">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3">Age</label>
                                    
                                    <div class="col-sm-9">
                                      <input type="text" class="form-control input-bg" placeholder="Age" name="age">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3">Gender</label>
                                    
                                    <div class="col-sm-9">
                                        <select class="form-control input-bg" name="gender">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3">Country</label>

                                    <div class="col-sm-9">
                                        <select class="form-control input-bg" name="country">
                                            <option value="india">India</option>
                                            <option value="us">US</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3">Subject</label>

                                    <div class="col-sm-9">
                                      <input class="form-control input-bg" placeholder="Subject" name="subject">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3">Public & private</label>
                                    
                                    <div class="col-sm-9">
                                        <label class="radio-inline">
                                            <input type="radio" name="type" value="public"> Public
                                        </label>

                                        <label class="radio-inline">
                                          <input type="radio" name="type" value="private"> Private
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3">Problem</label>
                                    
                                    <div class="col-sm-9">
                                        <textarea class="form-control input-bg" name="problem"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10 pull-right">
                                      <button type="submit" class="btn btn-warning pull-right" name="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <!--end contact form -->
                        </div>
                        <!--end contact_box -->
                    </div>
                    <!--end sevencol -->
                </div>
                <!--end twelvecol -->
            </div>
            <!--end row -->
        </div>
        <!--end contact_wrapper -->
    </section>
    <!--end contact section -->
</div>
<?php
}
function search_form_creation(){
 
//Adding scripts and css
wp_enqueue_script( 'bootstrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.js', array( 'jquery' ) );
wp_register_style( 'style-css', plugin_dir_url( __FILE__ ) . 'css/style.css' );
wp_enqueue_style('style-css');
wp_register_style( 'bootstrap-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap.css' );
wp_enqueue_style('bootstrap-css');

?>
<div id="dotted_bg"></div>
    
<!--begin container -->
<div id="container">

    <!--begin contact section -->
    <section id="contact">
            
        <!--begin contact_wrapper -->
        <div class="contact_wrapper">
            <!--begin row -->
            <div class="row">
                <!--begin twelvecol -->
                <div class="col-sm-12">
                    <!--begin sevencol -->
                    <div class="twelvecol">
                        <!--begin contact_box -->
                        <div class="col-sm-offset-2 col-sm-8">
                            <!--begin contact form -->
                            <form class="form-horizontal" action="<?php echo admin_url( 'admin.php' ); ?>" method="post">
                                <h4 class="orange">ANSWER SEARCH FORM</h4>
                                <div class="form-group">
                                   
                                    
                                    <div class="col-sm-9">
                                      <input type="email" class="form-control input-bg" placeholder="Email" name="email">
                                    </div>
                                    <div class="col-sm-3">
                                      <button type="submit" class="btn btn-warning pull-right" name="submit">Submit</button>
                                    </div>
                                </div>
                                
                                 
                            </form>
                            <!--end contact form -->
                        </div>
                        <!--end contact_box -->
                    </div>
                    <!--end sevencol -->
                </div>
                <!--end twelvecol -->
            </div>
            <!--end row -->
        </div>
        <!--end contact_wrapper -->
    </section>
    <!--end contact section -->
</div>
<?php
}

