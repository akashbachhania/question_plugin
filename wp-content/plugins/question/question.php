<?php
/**
 * @package Question
 */

/*
* Plugin Name: Question Plugin
* Description: Submit your questions and get answers.
* Version: 1.0
* Author: EagleHeights
* Author URI: http://EagleHeights.com
*/

include('question_functions.php');
include('settings.php');

/* Runs when plugin is activated to create answer page*/
register_activation_hook(__FILE__,'create_answer_page'); 

/* Runs on plugin deactivation*/
register_deactivation_hook( __FILE__, 'my_plugin_remove' );

//Submit form
add_action('admin_init', 'question_submit_form' );

add_shortcode('question_form', 'form_creation');

// answer form
add_shortcode('answer_question_form', 'search_form_creation');