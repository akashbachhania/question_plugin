<?php
function create_answer_page(){

    global $wpdb;

    $the_page_title = 'Answer Page';
    $the_page_name = 'Answer Page';

    // the menu entry...
    delete_option("answer_page_title");
    add_option("answer_page_title", $the_page_title, '', 'yes');
    
    // the slug...
    delete_option("answer_page_name");
    add_option("answer_page_name", $the_page_name, '', 'yes');
    
    // the id...
    delete_option("answer_page_id");
    add_option("answer_page_id", '0', '', 'yes');

    $the_page = get_page_by_title( $the_page_title );

    if ( ! $the_page ) {

        // Create post object
        $_p = array();
        $_p['post_title'] = $the_page_title;
        $_p['post_content'] = "[answer_question_form]";
        $_p['post_status'] = 'publish';
        $_p['post_type'] = 'page';
        $_p['comment_status'] = 'closed';
        $_p['ping_status'] = 'closed';
        $_p['post_category'] = array(1); // the default 'Uncatrgorised'

        // Insert the post into the database
        $the_page_id = wp_insert_post( $_p );

    }
    else {
        // the plugin may have been previously active and the page may just be trashed...

        $the_page_id = $the_page->ID;
        $the_page->post_content = "[answer_question_form]";
        //make sure the page is not trashed...
        $the_page->post_status = 'publish';
        $the_page_id = wp_update_post( $the_page );

    }
    $new_page_template = WP_PLUGIN_DIR .'/'. plugin_basename( dirname(__FILE__) ) . '/custom-page-template.php';
     if(!empty($new_page_template)){
                        update_post_meta($the_page_id, '_wp_page_template', $new_page_template);
    }

    delete_option( 'answer_page_id' );
    add_option( 'answer_page_id', $the_page_id );

}

function my_plugin_remove() {

    global $wpdb;

    $the_page_title = get_option( "answer_page_title" );
    $the_page_name = get_option( "answer_page_name" );

    //  the id of our page...
    $the_page_id = get_option( 'answer_page_id' );
    if( $the_page_id ) {

        wp_delete_post( $the_page_id ); // this will trash, not delete

    }

    delete_option("answer_page_title");
    delete_option("answer_page_name");
    delete_option("answer_page_id");

}
?>